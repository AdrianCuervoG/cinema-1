#include "funciones.h"
// Funciones de la practica 3 con algunas adaptaciones para el reto. aqui se validan los usuarios y el admin
string leer_Txt(int m){       // leer los textos encriptados  linea 3 hasta 25
    long long int tam;
    string texto, text;
    char s;
    if(m==1){
        text="../archivos/cartelera.dat";
    }
    else if (m==2) {
        text="../archivos/reservas.txt";
    }
    else if(m==3){
        text="../archivos/usuarios.dat";
    }
    else if (m==4) {
        text="../archivos/sudo.dat";
    }
                                               //Tamano del archivo  linea 14 hasta 17
    fstream k(text, ios::in | ios::ate);
    tam=k.tellg();
    k.close();
    //
    fstream k1(text, ios::in | ios::binary);    //paso el archivo al string texto
    for(long long int i=0;i<tam;i++){
        k1.get(s);
        texto.push_back(s);
    }
    k1.close();
    return texto;
}
void text2bin(string texto,bool *cod){    // Convierte el texto original a binario
    char s;
    for(unsigned long long int i=0; i<(texto.length()); i++){
        s=char(texto[i]);
        for(unsigned long long int j=0;j<8;j++){
            cod[8*i+j]=(((s<<j)&0x80)==0x80);
        }
    }
}
//separa dependiendo la semilla   linea 36 hasta 50
void separacion(unsigned long long int semilla, bool *cod, unsigned long long int tam, bool metodo,int m){
  short *seg = new short[semilla];
  long long int j=0;
  unsigned short tp=0;
  for(unsigned long long int i=0;i<semilla;i++) seg[i]=-1;
  for(unsigned long long int i=0,l=0; i<tam; i++,j++){
      seg[j]=cod[i];
      if((i+1)%semilla==0 || i==(tam-1)){
          tp=reglas(seg,&cod[semilla*l],tp,semilla,metodo,m);
          for(unsigned long long int k=0;k<semilla;k++) seg[k]=-1;
          l++;
          j=-1;
          }
  }
}
// organiza los bits dependiendo el metodo de codificacion o decodificacion  linea 51 hasta 102
unsigned short reglas(short *seg, bool *data, unsigned short regla, unsigned long long int semilla, bool metodo,int m){
    int contador[2]={0,0};
    unsigned long long int d=0;
    if(!metodo)d=2;
    unsigned short reg;
    if(m==1){                    // codificacion y decodificacion metodo 1   linea 57 hasta 79
        reg=0;
        for(unsigned long long int i=0; i<semilla; i++){
            if(seg[i]!=-1){
                switch (regla){
                    case 0: data[i]=1-seg[i];

                    break;

                    case 1: if(i%2!=0) data[i]=1-seg[i];

                    break;

                    default: if((i+1)%3==0) data[i]=1-seg[i];
                }

              if(metodo) contador[seg[i]]++;
              else contador[data[i]]++;
            }
        }
        if(contador[0]>contador[1]) reg=1;
        else if(contador[0]<contador[1]) reg=2;
    }
    else{                               //   codificacion y decodificacion del metodo 2  linea 80 hasta 100
        for (int i=0;i<semilla;i++) {
            if(seg[i]==-1){
                semilla=i;
                break;
            }
        }
        for(unsigned long long int i=0; i<semilla; i++){
            if(seg[i]!=-1){
                if(i==0){
                    if(!metodo){
                        reg=seg[i];
                        data[i]=seg[i+1];
                    }
                    else data[i]=seg[semilla-1];
                }
                else if(i==(semilla-1)&&!metodo)data[i]=reg;
                else data[i]=seg[i-1+d];
            }
        }
    }
    return reg;
}

string bin2text(string texto,bool *cod){      // pasa de binario con las modificaciones a texto linea 104 hasta 116
    int a,pot=1;
    for(unsigned long long int i=0; i<(texto.length()); i++){
        a=0;
        pot=1;
        for(unsigned long long int j=0;j<8;j++){
            a+=(cod[j+8*i]*128)/pot;
            pot*=2;
        }
        texto[i]=char(a);
    }
    return texto;
}
// escribe el texto a el archivo usuarios  linea 118 hasta 125  (solo el admin lo hace apartir de la 119)
void escribir_Txt(string texto,int m){
    string text;
    if(m==1){
        text="../archivos/cartelera.dat";
    }
    else if (m==2) {
        text="../archivos/reservas.txt";
    }
    else if(m==3){
        text="../archivos/usuarios.dat";
    }
    ofstream k2(text,  ios::out | ios::binary);
    k2 << texto;
    k2.close();
}
string aplicacion(){
    string texto,text;
    bool metodo=true;
    string clave,admi="",ced,din;
    int saldo=0,cant=0,possal=0,t1=0,t2=0,semilla,m;
    bool op;
    cout << "Ingrese el metodo de codificacion \n 1. metodo1 \n 2. metodo2    ";
    cin >> m;
    cout << "Ingrese la semilla: ";
    cin >> semilla;
    cout<<"ingrese su clave para ingresar: ";
    cin>>clave;
    admi=leer_Txt(4);
    bool *cod= new bool [8*admi.length()];
    text2bin(admi,cod);
    separacion(semilla,cod,8*admi.length(),!metodo,m);
    admi=bin2text(admi,cod);
    if(admi==clave)ced=admin(semilla,m);
    else{
        cout<<"ingrese su cedula: ";
        cin>>ced;
        texto=leer_Txt(3);//el 3 es del m
        bool *cod= new bool [8*texto.length()];
        text2bin(texto,cod);
        separacion(semilla,cod,8*texto.length(),!metodo,m);
        texto=bin2text(texto,cod);
        texto+='\n';
        for(int i=0;i<texto.length();i++){
            if(texto[i-1]=='\n'||i==0){
                for(int j=0;j<ced.length();i++,j++){
                    if((texto[i]==ced[j])){
                        if(j==t1){
                            t1++;
                        }
                    }
                    else{                        //prueba     para que no entre cualquiera parecido
                        if (j<(ced.length())/2) t1=0;

                    }
                }
                if(t1>3){
                    i++;
                    for(int j=0;j<clave.length();i++,j++){
                        if((texto[i]==clave[j])){
                            if(j==t2){
                                t2++;
                                possal=i+2;
                            }
                        }
                    }
                }
                else t1=0;
            }

        }
    }
    if(possal>5);
    else if(clave==admi);
    else {
        cout<<"Usuario no registrado por favor valide su ceudla o clave"<<endl;
        cout<<"En caso de no pertenecer comunicate con el Administrador"<<endl;
        ced=48;
    }
    return ced;
}
string admin(unsigned long long semilla,int m){
    bool metodo=1;
    int op;
    string text,var,texto;
    text="sudo.dat";
    cout <<"INGRESANDO COMO ADMINISTRADOR..."<<endl;
    cout<<"Que desea hacer:\n1.Registrar usuario\n2.Modificar cartelera\n3.Generar Reporte de ventas del dia"<<endl;
    cin>>op;
    if(op==1){
        cout << "Ingrese la semilla de codificacion de usuarios: ";
        cin >> semilla;
        texto="\r\n";
        cout << "Cedula: ";
        cin>>var;
        texto+=var;
        texto+=" ";
        cout << "Clave: ";
        cin>>var;
        texto+=var;
        texto+='\n';
        text=leer_Txt(3);
        bool *cod= new bool [8*text.length()];
        text2bin(text,cod);
        separacion(semilla,cod,8*text.length(), !metodo,m);   //metodo sera 0 para que decodifique
        text=bin2text(text,cod);
        text+=texto;                      //aqui le agrego lo nuevo        y lo vuelvo a codificar
        bool *codi= new bool [8*text.length()];
        text2bin(text,codi);
        separacion(semilla,codi,8*text.length(), metodo,m);
        text=bin2text(text,codi);
        escribir_Txt(text,3);
    }
    else if(op==2)return "2";
    else return "3";
}
