#include "cinema.h"
bool cinema::exist(int n){
    c=pos.find(n);
    return c!=pos.end();
}
void cinema::mostrar_sala(int sala){  //ingresan que sala quieren ver y si existe se imprime, cuando
    prueba();
    for(c=pos.begin();c!=pos.end();c++){
        if(c->first==sala){
            cout<<"    Sala numero  "<<c->first<<endl;
            cout<<"    1   2   3   4   5   6   7   8   9   10  11  12  13  14  15";
            c->second.mostrars();
            cout<<"\n  +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+"<<endl;
            cout<<"\n    ____________________________________________________";
            cout<<"\n    ----------------------------------------------------        __/ _";
            cout<<"\n                        PANTALLA                               Salida\n"<<endl;
        }
    }

}
void cinema::mostrar_cartelera(){
    string texto;
    int numcar[8]={2,4,19,10,10,9,11,4};  //medidas cartelera
    int n=0,c=0,m=0,a=0,cont=0;
    texto=leer_Txt(1);
    for (int i=0;i<8;i++) {          // cuento las medidas de mi cartelera
        n+=numcar[i];
    }
    for (int i=0;i<texto.length();i++) {     // cuantos id hay en cartelera
        if(texto[i]=='\n')cont++;
    }
    cout<<"\n    CARTELERA"<<endl;
    while(a<=cont){            //imprime cada linea de la cartelera
        for (int j=0,k=0;j<n;j++) {
            if(0==(j-m)){
                cout<<"+-";
                m+=numcar[k];
                k++;
            }
            else cout<<"-";

        }
        cout<<"+";
        if(c==0){
            cout<<"\n|ID|Tipo|      Nombre       |  Genero  | Duracion |Sala/Hora| Asientos  |Clas|";
            cout<<"\n|  |    |                   |          |          |         |Disponibles|    |"<<endl;
            c++;
            m=0;
        }
        else if((texto[c-2]=='\n')||(c<2)){//o fin de linea
            m=0;
            cout<<"\n|"<<texto[c-1]<<" |";
            c++;
            for (int j=1;j<8;j++) {
                for (int i=0;i<numcar[j];i++) {
                    if(texto[c+1]=='\n'&&texto[c]=='\r') {
                        cout<<" ";
                        j=9;
                        i=numcar[j];
                        c+=2;
                    }
                    else if(texto[c]=='-') cout<<" ";

                    else {
                        cout<<texto[c];
                        c++;
                    }
                }
                cout<<"|";
                c++;
            }
            cout<<endl;
            a++;

        }
        else break;
    }
    cout<<"+--+----+-------------------+----------+----------+---------+-----------+----+"<<endl;
}
int cinema::admin(string n){
    cout <<"INGRESANDO COMO ADMINISTRADOR..."<<endl;
    bool op;
    do{

        int numcar[8]={2,4,19,10,10,9,11,4};
        int porsi=0,d=0;
        string text,agg="",texto,txtcartelera;
        texto="\r\n";
        if(n=="2"){  //cartelera
            text=leer_Txt(1);
            for (int i=0;i<text.length();i++) {
                if(text[i]=='\n')porsi++;
            }
            porsi+=2;
            if(porsi>9){
                texto+="1";
                porsi-=10;
            }
            texto+=char(porsi+48);
            texto+="-";
            mostrar_cartelera();
            for (int i=0;i<7;i++) {
                do{
                    agg="";
                    d=numcar[i+1];
                    if(i==0)cout<<"ingrese tipo de la pelicula 3D o 2D"<<endl;
                    else if(i==1)cout<<"ingrese el nombre de la nueva pelicula"<<endl;
                    else if(i==2)cout<<"ingrese el genero de la pelicula"<<endl;
                    else if(i==3)cout<<"ingrese la duracion. ej: 125 min"<<endl;
                    else if(i==4)cout<<"ingrese la sala y hora debe de ser una sala nueva ej: 5/10pm = seria sala 5 a las 10pm"<<endl;
                    if(i==5){
                        texto+="0/120-";
                        i++;
                        cout<<"ingrese la edad minima para poder ingresar a la pelicula  ej:16+"<<endl;
                        cin>>agg;
                    }
                    else {
                        getline(cin,agg);
                    }
                    if(agg.length()>d)cout<<"Verifique su dato, esta muy lago o esta incorrecto"<<endl;
                }while(agg.length()>d);
                texto+=agg;
                texto+="-";
            }
            text+="\n";
            text+=texto;
            escribir_Txt(text,1);
            mostrar_cartelera();
        }
        else{     // reporte de ventas
            int ven2dg=0,ven2dp=0,ven2dv=0,ven3dg=0,ven3dp=0,ven3dv=0;
            text=leer_Txt(2);
            cout<<"\n   Reporte De Ventas..."<<endl;
            for (int i=0;i<text.length();i++) {
                if(text[i]=='D'&&text[i-1]=='3'){
                    d++;
                    if(text[i+2]=='1')ven3dg+=10800;
                    if(text[i+2]=='2')ven3dp+=11800;
                    if(text[i+2]=='3')ven3dv+=12900;
                }
                else if(text[i]=='D'&&text[i-1]=='2'){
                    if(text[i+2]=='1')ven2dg+=7900;
                    if(text[i+2]=='2')ven2dp+=8900;
                    if(text[i+2]=='3')ven2dv+=9900;
                }
                if(text[i]=='\n')porsi++;
            }
            porsi++;
            cout<<"Boletas Tipo 3D vendidas: "<<d<<endl;
            cout<<"Genreal 3D vendidas: "<<(ven3dg/10800)<<" para un saldo de: $"<<ven3dg<<endl;
            cout<<"Preferencial 3D vendidas: "<<(ven3dp/11800)<<" para un saldo de: $"<<ven3dp<<endl;
            cout<<"VibroSound 3D vendidas: "<<(ven3dv/12900)<<" para un saldo de: $"<<ven3dv<<endl;
            cout<<"Saldo total Boletas 3D: $"<<(ven3dg+ven3dp+ven3dv)<<endl<<endl;
            cout<<"Boletas Tipo 2D vendidas: "<<(porsi-d)<<endl;
            cout<<"Genreal 2D vendidas: "<<(ven2dg/7900)<<" para un saldo de: $"<<ven2dg<<endl;
            cout<<"Preferencial 2D vendidas: "<<(ven2dp/8900)<<" para un saldo de: $"<<ven2dp<<endl;
            cout<<"VibroSound 2D vendidas: "<<(ven2dv/9900)<<" para un saldo de: $"<<ven2dv<<endl;
            cout<<"Saldo total Boletas 2D: $"<<(ven2dg+ven2dp+ven2dv)<<endl<<endl;
            cout<<"\nTotal de Boletas vendidas : "<<porsi<<endl;
            cout<<"\nSaldo Total : $"<<(ven3dg+ven3dp+ven3dv)+(ven2dg+ven2dp+ven2dv)<<endl;
        }
        cout<<texto;
        cout << "\ningrese 0 para modificar otro dato o presione 1 para salir\n";
        cin >> op;
        if(!op){
            cout<<"Que deseas?  \n modificar cartelera presione 2\n reporte de ventas presione 3";
            cin>>n;
        }
    }while(!op);
}
void cinema::usuario(string ced){
    int as=0;
    cout<<"\nIngresando como Usuario...\n"<<endl;
    do{
        int id,porsi,ca,cb,cc,cf=0,cp,col;  //  ca=pracio general    cb=precio preferencial  cc=precio VIP  cf=total a pagar
        int sala=0,din=0,dev=0,dimension=0;                        // col=numero asiento  cp=valor entada
        char fila;
        bool t;
        string text;
        prueba();
        mostrar_cartelera();
        text=leer_Txt(1);
        for (int i=0;i<text.length();i++) {
            if(text[i]=='\n')porsi++;
        }
        do{
            cout<<"\n Te damos la bienvenida, que pelicula deseas reservar? ingrese el numero de ID"<<endl;
            cin>>id;
            if(porsi<id)cout<<"La ID de la pelicula no existe, porfavor verifica en la cartelera"<<endl;
        }while(porsi<id);
                  //Elector de asientos, los que quiera
        cout<<"Felicidades tu eleccion fue\n";
        for (int i=0;i<text.length();i++) {  ///a qui muestro nobre de la pelicula o toda la linea de datos
            if(id==1||text[i-1]=='\n'){
                if(id>9){
                    as=((int(text[i])-48)*10)+(int(text[i+1])-48);
                    i++;
                }
                else as=int(text[i])-48;
                i++;
                if(as==id){
                    dimension=int(text[i+1])-48;
                    cout<<"    "<<id<<"  ";
                    for (int j=i;j<text.length();j++,i++) {
                        if(text[j]=='-')cout<<"  ";
                        else if(text[j]=='\n')break;
                        else cout<<text[j];
                        if((text[j]=='/')&&((text[j-3]=='n'||text[j-4]=='n'))){
                            if(text[j-3]=='-')sala=((int(text[i-2])-48)*10)+(int(text[i-1])-48);
                            else sala=int(text[i-1])-48;
                        }
                    }
                }
            }
        }
        if(as==2){
            cout<<"\nCostos de sala... "<<endl;
            cout<<"+---+---------------------------------------+\n|   |  Clasificacion      Costo   Ubicacion |\n";
            cout<<"+---+---------------------------------------+\n| 1 |  General 3D        10.800   Fila F-G-H|\n";
            cout<<"| 2 |  Preferencial 3D   11.800   Fila C-D-E|\n| 3 |  VibroSound 3D     12.900   Fila  A-B |\n";
            cout<<"+----+---------------------------------------+"<<endl;
            ca=10800;
            cb=11800;
            cc=12900;
        }
        else{
            cout<<"\nCostos de sala... "<<endl;
            cout<<"+---+---------------------------------------+\n|   |  Clasificacion      Costo   Ubicacion |\n";
            cout<<"+---+---------------------------------------+\n| 1 |  General 2D         7.900   Fila F-G-H|\n";
            cout<<"| 2 |  Preferencial 2D    8.900   Fila C-D-E|\n| 3 |  VibroSound 2D      9.900   Fila  A-B |\n";
            cout<<"+----+--------------------------------------+"<<endl;
            ca=7900;
            cb=8900;
            cc=9900;
        }
        text=leer_Txt(2);
        mostrar_sala(sala);// aqui se debe mostrar ya con el puesto modificado
        cout<<"\nAsientos: Los asientos disponibles tienen un -, y los que yas estan reservados tienen un +"<<endl;
        do{
            cout<<"\nIngresa la letra de fila que desea:"<<endl;
            cin.ignore(); cin>>fila;
            if(int(fila)<123&&int(fila)>96)fila=fila-32;
            if(sil.exist(fila)){
                if(fila=='A'||fila=='B'){
                    cout<<" el costo de esta fila es: "<<cc<<endl;
                    cp=cc;
                    cf+=cc;
                    porsi=3;
                }
                else if(fila=='C'||fila=='D'||fila=='E'){
                    cout<<" el costo de esta fila es: "<<cb<<endl;
                    cp=cb;
                    cf+=cb;
                    porsi=2;
                }
                else {
                    cout<<" el costo de esta fila es: "<<ca<<endl;
                    cp=ca;
                    cf+=ca;
                    porsi=1;
                }
            }
        }while(!sil.exist(fila));
        do{
            cout<<"Elije el numero de el asiento:"<<endl;
            cin>>col;
            if(col>15)cout<<"Numero de asiento incorrecto, verifica el numero"<<endl;
        }while(col>15);
        t=reservar(sala,fila,col);
        if(t){
            cout<<"...Reserva exitosa...\n";
            text+="\n";
            for (int i=0,num=0;i<6;i++) {
                num=0;
                if(i==0)num=id;
                else if(i==1)num=sala;
                else if(i==2){
                    text+=dimension+48;
                    text+="D";}
                else if(i==3)num=porsi;
                else if(i==4)text+=fila;
                else if(i==5)num=col;
                if(num>9){
                    text+=char((num/10)+48);
                    num%=10;
                    text+=char(num+48);
                }
                else if(num!=0) text+=char(num+48);
                text+="-";
                if(i==5){
                    text+=ced;
                    text+="\r\n";
                    break;
                }
            }
            escribir_Txt(text,2);
        }
        else{ cout<<"Asiento ocupado, elije uno libre\n";cf=0;}
        if(cf>50){
            do{
                cout<<"La deuda total es de : $"<<cf<<"ingrese aqui el dinero y espere su cambio."<<endl;
                cin>>din;
                if(din<cf)cout<<"saldo ingresado insuficiente, intentalo de nuevo"<<endl;
            }while(din<cf);
            din-=cf;
            do{
                if(din>50000)dev+=50000;
                else if(din>=20000){dev+=20000;din-=20000;}
                else if(din>=10000){dev+=10000;din-=10000;}
                else if(din>=5000){dev+=5000;din-=5000;}
                else if(din>=2000){dev+=2000;din-=2000;}
                else if(din>=1000){dev+=1000;din-=1000;}
                else if(din>=500){dev+=500;din-=500;}
                else if(din>=200){dev+=200;din-=200;}
                else if(din>=100){dev+=100;din-=100;}
                else if(din>=50){dev+=50;din-=50;}
            }while(din>50);
            cout<<"Te devuelvo : $"<<dev<<endl;
        }
        cout<<"Ingrese 1 para hacer otra reserva con el mismo usuario,\n o ingrese 0 pagar todo y salir"<<endl;
        cin>>as;
    }while(as==1);//por si desea reservar mas

}
void cinema::prueba(){
    int n=0,numsala=0;
    string text;
    text=leer_Txt(1);
    for (int i=0;i<text.length();i++) {
        if(text[i]=='\n')numsala++;
    }
    numsala++;
    int sala[numsala];                ///  agrogo las salas ya creadas en mi archivo cartelera
    for (int i=0;i<text.length();i++){
        if((text[i]=='/')&&(int(text[i+3])>64)){
            if(text[i-2]=='-'){
                sala[n]=int(text[i-1]-48);
            }
            else sala[n]=(int(text[i-2]-48)*10)+(int(text[i-1])-48);
            n++;
        }
    }

    for(int i=0;i<numsala;i++){
        sil.crear2();
        pos.insert(pair<int,sillas>(sala[i],sil));
    }
    llenar_reservaSala();
}
bool cinema::llenar_reservaSala(){
    string text;
    bool t;
    int sala,col;
    char fila;
    text=leer_Txt(2);
    for (int i=0;i<text.length();i++) {
        if(i==0||text[i-1]=='\n'){
            if(text[i+1]=='-')i+=2;      //aqui verifico si la id es mayor de 9 o de 99
            else if(text[i+2]=='-') i+=3;
            else i+=4;
            if(text[i+1]!='-'){        //aqui verifico si el numero de la sala es mayor o menor de 9
                sala=(int(text[i]-48)*10)+(int(text[i+1])-48);
                i++;
            }
            else sala=(int(text[i])-48);
            i+=9; //aqui omito 2d o 3d y el costo
            fila=text[i-2];   //agrego letra de fila
            if(text[i+1]=='-')col=(int(text[i])-48);
            else{
                col=(int(text[i]-48)*10)+(int(text[i+1])-48);
                i++;
            }
            t=reservar(sala,fila,col);        //aqui agrego
        }
    }
    return 0;
}
bool cinema::reservar(int sala,char fila,int col){
    bool t;
    for(c=pos.begin();c!=pos.end();c++){
        if(c->first==sala){
            t=sil.reservar2(fila,col);                      //si sale 1 se modico, si sale 0 ya estaba lleno
            if(t){
                pos[sala]=sil;
            }
        }
    }
    return t;
}
