#include <iostream>
#include <fstream>

using namespace std;

string leer_Txt(int m);
void text2bin(string texto,bool *cod);
void separacion(unsigned long long int semilla, bool *cod, unsigned long long int tam, bool codify,int m);
unsigned short reglas(short *seg, bool *data, unsigned short regla, unsigned long long int semilla, bool codify,int m);
string bin2text(string texto,bool *cod);
void escribir_Txt(string texto,int m);
long long int tamano(char *name);
string aplicacion();
string admin(unsigned long long semilla,int m);
