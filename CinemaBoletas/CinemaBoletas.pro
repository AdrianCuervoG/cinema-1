TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        cinema.cpp \
        funciones.cpp \
        main.cpp \
        sillas.cpp \
        ubicacion.cpp

HEADERS += \
    cinema.h \
    funciones.h \
    sillas.h \
    ubicacion.h
