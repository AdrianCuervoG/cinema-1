#include <sillas.h>
#include <funciones.h>
using namespace std;
class cinema
{
public:
    void mostrar_sala(int sala);
    void mostrar_cartelera();
    int admin(string n);
    void usuario(string ced);
    void prueba();                   //crea la sala con base en la cartelera y las reservas
    bool llenar_reservaSala();
    bool reservar(int sala,char fila,int col);
private:
    sillas sil;
    map<int,sillas> pos;
    map<int,sillas> :: iterator c;
    bool exist(int n);
};
